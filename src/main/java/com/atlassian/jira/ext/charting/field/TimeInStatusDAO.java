package com.atlassian.jira.ext.charting.field;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.status.Status;
import org.apache.commons.collections.MultiMap;
import org.apache.commons.collections.map.ListOrderedMap;
import org.apache.commons.collections.map.MultiValueMap;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.ofbiz.core.entity.DelegatorInterface;
import org.ofbiz.core.entity.jdbc.SQLProcessor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Used to calculate the amount of time spent is a specified status
 */
public class TimeInStatusDAO
{
    public static final String STATUSES_SEPARATOR = "_*|*_";
    public static final String STATUS_VALUES_SEPARATOR = "_*:*_";

    String ENTITY_DS = "defaultDS"; // this is package private so that it can be overridden in tests
    private static final Logger log = Logger.getLogger(TimeInStatusDAO.class);
    protected final DelegatorInterface delegatorInterface;
    private String selectStatement;

    private static final String CHANGEGROUP_TABLE_ALIAS = "cg";
    private static final String CHANGEITEM_TABLE_ALIAS = "ci";

    protected static ConstantsManager constantsManager;

    protected TimeInStatusDAO(DelegatorInterface delegatorInterface)
    {
        this.delegatorInterface = delegatorInterface;
        String changeGroup = getTableName("ChangeGroup");
        String changeItem = getTableName("ChangeItem");
        String cg_created = getColName("ChangeGroup", "created");
        String cg_issueid = getColName("ChangeGroup", "issue");
        String cg_id = getColName("ChangeGroup", "id");
        String ci_field = getColName("ChangeItem", "field");
        String ci_oldvalue = getColName("ChangeItem", "oldvalue");
        String ci_newvalue = getColName("ChangeItem", "newvalue");
        String ci_fieldtype = getColName("ChangeItem", "fieldtype");
        String ci_gid = getColName("ChangeItem", "group");

        selectStatement = "select " + CHANGEGROUP_TABLE_ALIAS + "." + cg_created + ", " +
                CHANGEITEM_TABLE_ALIAS + "." + ci_oldvalue + ", " +
                CHANGEITEM_TABLE_ALIAS + "." + ci_newvalue + " " +
                " from " + changeGroup + " " + CHANGEGROUP_TABLE_ALIAS + " , " + 
                        changeItem + " " + CHANGEITEM_TABLE_ALIAS +
                " where " + CHANGEGROUP_TABLE_ALIAS + "." + cg_issueid + " = ? " +
                " and " + CHANGEGROUP_TABLE_ALIAS + "." + cg_id + " = " + CHANGEITEM_TABLE_ALIAS + "." + ci_gid +
                " and " + CHANGEITEM_TABLE_ALIAS + "." + ci_fieldtype + "='jira' " +
                " and " + CHANGEITEM_TABLE_ALIAS + "." + ci_field + "='status' " +
                " order by " + CHANGEGROUP_TABLE_ALIAS + "." + cg_created + " asc";
    }

    /**
     * This is a select statement, that takes one parameter (an issue id), and returns a single Timestamp column
     *
     * @return A select statement that returns a single column, the date field that you wish
     */
    protected String getSelectStatement()
    {
        return selectStatement;
    }

    protected String getTableName(String entityName)
    {
        return delegatorInterface.getModelEntity(entityName).getTableName(ENTITY_DS);
    }

    protected String getColName(String entityName, String fieldName)
    {
        return delegatorInterface.getModelEntity(entityName).getField(fieldName).getColName();
    }

    public String calculateForStatuses(Issue issue)
    {
        String statusSummary = null;
        SQLProcessor sqlProcessor = null;
        ResultSet resultSet;

        try
        {
            sqlProcessor = getSQLProcessor();
            sqlProcessor.prepareStatement(getSelectStatement());
            sqlProcessor.setValue(issue.getId());

            sqlProcessor.executeQuery();
            resultSet = sqlProcessor.getResultSet();

            Map results = processResultSet(resultSet, issue);
            if (!results.isEmpty())
            {
                statusSummary = doCalculation(results);
            }
        }
        catch (Throwable e)
        {
            log.error(e, e);
        }
        finally
        {
            if (sqlProcessor != null)
            {
                try
                {
                    // Closing the SQLProcessor closes the ResultSet as well.
                    sqlProcessor.close();
                }
                catch (Exception e)
                {
                    log.error(e, e);
                }
            }
        }
        return statusSummary;
    }

    protected SQLProcessor getSQLProcessor() {
        return new SQLProcessor(ENTITY_DS);
    }

    public static Double getNumberOfTimesInStatus(String status, String customFieldValue)
    {
        return getValueFromStatusCustomField(status, customFieldValue, true);
    }

    public static Double getSecondsInStatus(String status, String customFieldValue)
    {
        return getValueFromStatusCustomField(status, customFieldValue, false);
    }

    private static Double getValueFromStatusCustomField(String statusId, String customFieldValue, boolean numberOfTimesIn)
    {
        String [] statuses = StringUtils.splitByWholeSeparator(customFieldValue, STATUSES_SEPARATOR);
        Map statusNameMap = getStatusNameMap();

        for (int i = 0; i < statuses.length; i++)
        {
            String statusString = statuses[i];
            String [] vals = StringUtils.splitByWholeSeparator(statusString, STATUS_VALUES_SEPARATOR);
            if (vals != null && vals.length == 3)
            {
                String currentStatus = vals[0];
                String currentStatusId;
                //if status !Number assume that it is a status name and convert to a statusId
                if(isInteger(currentStatus))
                {
                    currentStatusId = currentStatus;
                }
                else
                {
                    if (statusNameMap.containsKey(currentStatus))
                        currentStatusId = ((Status)statusNameMap.get(currentStatus)).getId();
                    else
                        continue; /* Status cannot be determined, skip */
                }


                String numberOfTimesInStatus = vals[1];
                String secondsInStatus = vals[2];

                if (currentStatusId.equals(statusId))
                {
                    if (numberOfTimesIn)
                    {
                        return new Double(numberOfTimesInStatus);
                    }
                    else
                    {
                        return new Double(secondsInStatus);
                    }
                }
            }
        }

        return null;
    }

    private String doCalculation(Map results)
    {
        Timestamp previousStart = null;
        String previousStatus = null;

        MultiMap numberOfTimesInStatuses = new MultiValueMap();
        Map secondsInStatuses = new LinkedHashMap();

        // Use the previousStart as an indicator that we need to record the amount
        // of time in the last state
        for (Iterator iterator = results.keySet().iterator(); iterator.hasNext();)
        {
            Timestamp startTime = (Timestamp) iterator.next();
            String dbStatus = (String) results.get(startTime);

            // record that we have hit the status
            numberOfTimesInStatuses.put(dbStatus, "");

            // If we have set the previous start in a earlier loop then we must
            // record the amount of time spent in that state
            if (previousStart != null && previousStatus != null)
            {
                Long secondsInStatusLong = (Long) secondsInStatuses.get(previousStatus);
                long secondsInStatus = (secondsInStatusLong == null) ? 0 : secondsInStatusLong.longValue();

                secondsInStatus += startTime.getTime() - previousStart.getTime();
                secondsInStatuses.put(previousStatus, new Long(secondsInStatus));
            }

            // If we are in a matching status then we set the previousStart this this
            // start time, otherwise we make sure we have null'ed out the value.
            previousStart = startTime;
            previousStatus = dbStatus;
        }
        return buildCustomFieldValueFromData(numberOfTimesInStatuses, secondsInStatuses);
    }

    private String buildCustomFieldValueFromData(MultiMap numberOfTimesInStatuses, Map secondsInStatuses)
    {
        StringBuffer result = new StringBuffer();
        boolean first = true;

        for (Iterator iterator = numberOfTimesInStatuses.keySet().iterator(); iterator.hasNext();)
        {
            String statusId = (String) iterator.next();
            int numberOfTimesInStatus = ((Collection) numberOfTimesInStatuses.get(statusId)).size();
            Long secondsInStatusLong = (Long) secondsInStatuses.get(statusId);
            long secondsInStatus = (secondsInStatusLong != null) ? secondsInStatusLong.longValue() : 0;

            if (!first)
            {
                result.append(STATUSES_SEPARATOR);
            }
            else
            {
                first = false;
            }

            result.append(statusId);
            result.append(STATUS_VALUES_SEPARATOR);
            result.append(numberOfTimesInStatus);
            result.append(STATUS_VALUES_SEPARATOR);
            result.append(secondsInStatus);

        }
        return result.toString();
    }

    private Map processResultSet(ResultSet resultSet, Issue issue) throws SQLException
    {
        boolean first = true;
        Map timeInStatusesMap = new ListOrderedMap();
        while (resultSet.next())
        {
            Timestamp startTime = resultSet.getTimestamp(1);
            String oldString = resultSet.getString(2);
            String newString = resultSet.getString(3);
            // Treat the first status as special since we use the issue create date as the start time
            // and the old value of the change log as the initial state.
            if (first)
            {
                timeInStatusesMap.put(issue.getCreated(), oldString);
                first = false;
            }

            // Always store the new value and timestamp as a transition entry
            timeInStatusesMap.put(startTime, newString);
        }
        return timeInStatusesMap;
    }

    /**
     * Converts a list of status names to a list of status ids.
     * The method checks to see whether the first item of the list
     * is numeric. If it is then it assumes that the list consists
     * status ids and just returns it.
     * @param statusNames
     */
    public static List<String> convertNamesToStatusIds(List<String> statusNames)
    {
        if((statusNames.size() > 0) && (!isInteger(statusNames.get(0))))
        {
            Map<String, Status> statusMap = getStatusNameMap();
            List<String> statusIds = new ArrayList<String>();

            for (String statusName : statusNames)
            {
                if (statusMap.containsKey(statusName))
                    statusIds.add(statusMap.get(statusName).getName());
            }
            return statusIds;
        }
        else
        {
            // This is already a list of Ids and doesn't require conversion
            return statusNames;
        }
    }

    public static Map<String, Status> getStatusNameMap()
    {
        ConstantsManager constantsManager = getConstantsManager();
        Map<String, Status> statusMap = new HashMap<String, Status>();

        for (Status status : constantsManager.getStatusObjects())
            statusMap.put(status.getName(), status);
        
        return statusMap;
    }

    protected static ConstantsManager getConstantsManager() {
        if (null == constantsManager)
        {
            constantsManager = ComponentAccessor.getConstantsManager();
        }
        return constantsManager;
    }

    public static Map<String, Status> getStatusIdMap()
    {
        ConstantsManager constantsManager = getConstantsManager();
        Map<String, Status> statusMap = new HashMap<String, Status>();
        for (Iterator iterator = constantsManager.getStatusObjects().iterator(); iterator.hasNext();)
        {
            Status status = (Status) iterator.next();
            statusMap.put(status.getId(), status);
        }
        return statusMap;
    }

    public static boolean isInteger(String str)
    {
        try
        {
            Integer.parseInt(str);
            return true;
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
    }
}
