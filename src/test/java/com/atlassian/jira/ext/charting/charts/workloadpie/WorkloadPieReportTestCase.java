package com.atlassian.jira.ext.charting.charts.workloadpie;

import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.bc.issue.worklog.TimeTrackingConfiguration;
import com.atlassian.jira.charts.Chart;
import com.atlassian.jira.charts.util.ChartUtils;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.issue.search.ReaderCache;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchProvider;
import com.atlassian.jira.plugin.report.ReportModuleDescriptor;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.JiraDurationUtils;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.atlassian.jira.web.action.ProjectActionSupport;
import junit.framework.TestCase;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.atlassian.jira.ext.charting.gadgets.charts.WorkloadPieChart.KEY_TOTAL_WORK_HOURS;
import static org.mockito.Mockito.when;

public class WorkloadPieReportTestCase extends TestCase
{
    private WorkloadPieReport workloadPieReport;
    
    private String htmlReport;
    private HashMap<String, Object> reportParams;
    private Map<String, String> reqParams;
    
    private String projectOrFilterId;
    private String statistictype;
    private String issuetimetype;
    private String i18nText;
    private String html;
    private Boolean option = true;
    
    @Mock private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock private ApplicationProperties applicationProperties;
    @Mock private ProjectManager projectManager;
    @Mock private SearchRequestService searchRequestService;
    @Mock private ChartUtils chartUtils;
    @Mock private CustomFieldManager customFieldManager;
    @Mock private ConstantsManager constantsManager;
    @Mock private IssueIndexManager issueIndexManager;
    @Mock private SearchProvider searchProvider;
    @Mock private SearchService searchService;
    @Mock private FieldManager fieldManager;
    @Mock private FieldVisibilityManager fieldVisibilityManager;
    @Mock private ReaderCache readerCache;
    @Mock private TimeTrackingConfiguration timeTrackingConfiguration;
    @Mock private ProjectActionSupport action;
    @Mock private JiraDurationUtils jiraDurationUtils;
    @Mock private I18nHelper i18nHelper;
    @Mock private ReportModuleDescriptor descriptor;
    @Mock private Chart chart;
    @Mock private VelocityRequestContextFactory velocityRequestContextFactory;
    
    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        
        MockitoAnnotations.initMocks(this);
        
        htmlReport = "";
        html = "<html><body>This is the finishing result of the process</body></html>";
        
        reportParams = new HashMap<String, Object>();
        reqParams = new HashMap<String, String>();
        
        statistictype = "line";
        issuetimetype = "hhmmss";
        i18nText = "Workload Pie";
        
        workloadPieReport = new WorkloadPieReport(jiraAuthenticationContext, applicationProperties, 
                projectManager, searchRequestService, chartUtils, customFieldManager, constantsManager, 
                issueIndexManager, searchProvider, searchService, fieldManager, fieldVisibilityManager, 
                readerCache, timeTrackingConfiguration, velocityRequestContextFactory)
        {
            @Override
            protected JiraDurationUtils getJiraDurationUtils()
            {
                return jiraDurationUtils;
            }

            @Override
            protected HashMap<String, Object> getHashMap()
            {
                return reportParams;
            }

            @Override
            protected Chart newWorkloadPieChart(
                    Map<String, Object> reportParams, String projectOrFilterId,
                    String statisticType, String issueTimeType)
                    throws SearchException, IOException
            {
                return chart;
            }
        };
        workloadPieReport.init(descriptor);
    }
    
    public void testGenerateHtmlReportWithNoProjectOrFilterId() throws Exception
    {
        projectOrFilterId = "";
        
        prepareConditions();
        
        htmlReport = workloadPieReport.generateReportHtml(action, reqParams);
        
        commonAssertions();
    }

    private void commonAssertions()
    {
        assertTrue(htmlReport.equals(html));
        assertTrue(reportParams.get("report") instanceof WorkloadPieReport);
        assertEquals(action, reportParams.get("action"));
        assertTrue((Boolean) reportParams.get("indexing"));
        assertNull(reportParams.get("user"));
    }
    
    public void testGenerateHtmlReportWithProjectOrFilterId() throws Exception
    {
        String hours = "800";
        
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put(KEY_TOTAL_WORK_HOURS, Long.parseLong(hours));
        
        projectOrFilterId = "Project Fate";
        
        prepareConditions();
        
        when(chart.getParameters()).thenReturn(parameters);
        when(jiraDurationUtils.getShortFormattedDuration((Long) parameters.get(KEY_TOTAL_WORK_HOURS))).thenReturn(hours);
        
        htmlReport = workloadPieReport.generateReportHtml(action, reqParams);
        
        commonAssertions();
        assertEquals(statistictype, reportParams.get("statisticType"));
        assertEquals(issuetimetype, reportParams.get("issueTimeType"));
        assertTrue(parameters.get(KEY_TOTAL_WORK_HOURS).equals(Long.parseLong(hours)));
    }
    
    protected void prepareConditions()
    {
        reqParams.put("projectOrFilterId", projectOrFilterId);
        reqParams.put("statistictype", statistictype);
        reqParams.put("issuetimetype", issuetimetype);
        
        when(applicationProperties.getOption(APKeys.JIRA_OPTION_INDEXING)).thenReturn(option);
        when(applicationProperties.getOption(APKeys.JIRA_OPTION_TIMETRACKING)).thenReturn(option);
        when(jiraAuthenticationContext.getI18nHelper()).thenReturn(i18nHelper);
        when(i18nHelper.getText("report.workloadpie.enableTimeTracking")).thenReturn(i18nText);
        when(descriptor.getHtml("view", reportParams)).thenReturn(html);
    }
}
