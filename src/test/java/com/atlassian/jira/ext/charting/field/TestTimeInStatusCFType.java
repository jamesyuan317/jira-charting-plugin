package com.atlassian.jira.ext.charting.field;


import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.converters.DatePickerConverter;
import com.atlassian.jira.issue.customfields.converters.DatePickerConverterImpl;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.persistence.PersistenceFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.mock.ofbiz.MockGenericValue;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.EasyList;
import junit.framework.TestCase;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.ofbiz.core.entity.DelegatorInterface;

import java.util.ArrayList;
import java.util.Collections;

import static org.mockito.Mockito.*;

public class TestTimeInStatusCFType extends TestCase {

    private String timeInStatusToReturnByTimeInStatusDAO;
    
    private DatePickerConverter datePickerConverter;

    @Mock private CustomFieldValuePersister customFieldPersister;
    @Mock private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock private Issue issue;
    @Mock private CustomField customField;
    @Mock private DelegatorInterface genericDelegator;

    protected void setUp() throws Exception {
        super.setUp();

        MockitoAnnotations.initMocks(this);
        new MockComponentWorker()
                .addMock(DelegatorInterface.class, genericDelegator)
                .init();

        datePickerConverter = new DatePickerConverterImpl(jiraAuthenticationContext, null);
    }

    public void getValueForStatuses() {
        timeInStatusToReturnByTimeInStatusDAO = "foo";
        assertEquals("foo", new TimeInStatusCFType(datePickerConverter, customFieldPersister).getValueForStatuses(null));
    }

    public void testGetValueFromIssueResolvedWithoutTimeInStatusInDb() {
        final Long issueId = new Long(1);
        final TimeInStatusCFType timeInStatusCFType;

        final String dbTimeInStatus = null;
        
        timeInStatusToReturnByTimeInStatusDAO = "foo";

        when(issue.getId()).thenReturn(issueId);
        when(issue.getResolution()).thenReturn(new MockGenericValue("IssueResolution"));

        when(customFieldPersister.getValues(customField, issueId, PersistenceFieldType.TYPE_UNLIMITED_TEXT)).thenReturn(new ArrayList<Object>(EasyList.build(dbTimeInStatus)));
        
        timeInStatusCFType = new TimeInStatusCFType(datePickerConverter, customFieldPersister);
        
        assertEquals(timeInStatusToReturnByTimeInStatusDAO, timeInStatusCFType.getValueFromIssue(customField,  issue));
        
        verify(issue, times(2)).getId();
        verify(issue).getResolution();
        verify(customFieldPersister).getValues(customField, issueId, PersistenceFieldType.TYPE_UNLIMITED_TEXT);
        verify(customFieldPersister).updateValues(customField, issueId, PersistenceFieldType.TYPE_UNLIMITED_TEXT, EasyList.build(timeInStatusToReturnByTimeInStatusDAO));
    }

    public void testRemove() {
        final TimeInStatusCFType timeInStatusCFType;

        when(customField.getId()).thenReturn("customfield_10000");
        when(customFieldPersister.removeAllValues(customField.getId())).thenReturn(Collections.EMPTY_SET);
        
        timeInStatusCFType = new TimeInStatusCFType(datePickerConverter, customFieldPersister);
        timeInStatusCFType.remove(customField);
        
        verify(customField, times(2)).getId();
        verify(customFieldPersister, times(2)).removeAllValues(customField.getId());
    }

    public void testGetStringFromSingularObject() {
        final String expectedString = "foo";
        final TimeInStatusCFType timeInStatusCFType = new TimeInStatusCFType(datePickerConverter, customFieldPersister);
        
        assertEquals(expectedString, timeInStatusCFType.getStringFromSingularObject("foo"));
    }

    public void testGetSingularObjectFromString() {
        final String expectedString = "foo";
        final TimeInStatusCFType timeInStatusCFType = new TimeInStatusCFType(datePickerConverter, customFieldPersister);
        
        assertEquals(expectedString, timeInStatusCFType.getSingularObjectFromString("foo"));
        assertNull(timeInStatusCFType.getSingularObjectFromString(null));
    }

    public void retrieveDatabaseValueWhenThereIsNoValue() {
        final Long issueId = new Long(1);
        final TimeInStatusCFType timeInStatusCFType;

        when(customFieldPersister.getValues(customField, issueId, PersistenceFieldType.TYPE_UNLIMITED_TEXT)).thenReturn(Collections.emptyList());
        when(issue.getId()).thenReturn(issueId);

        timeInStatusCFType = new TimeInStatusCFType(datePickerConverter, customFieldPersister);

        assertNull(timeInStatusCFType.retrieveDatabaseValue(customField, issue));
    }

    public void retrieveDatabaseValueWhenThereAreMoreThanOneValue() {
        final Long issueId = new Long(1);
        final TimeInStatusCFType timeInStatusCFType;

        when(customFieldPersister.getValues(customField, issueId, PersistenceFieldType.TYPE_UNLIMITED_TEXT)).thenReturn(new ArrayList<Object>(EasyList.build("foo", "bar")));
        
        timeInStatusCFType =  new TimeInStatusCFType(datePickerConverter, customFieldPersister);

        assertNull(timeInStatusCFType.retrieveDatabaseValue(customField, issue));
    }

    public void retrieveDatabaseValueWhenThereIsOnlyOneValue() {
        final Long issueId = new Long(1);
        final TimeInStatusCFType timeInStatusCFType;

        when(customFieldPersister.getValues(customField, issueId, PersistenceFieldType.TYPE_UNLIMITED_TEXT)).thenReturn(new ArrayList<Object>(EasyList.build("foo")));

        timeInStatusCFType = new TimeInStatusCFType(datePickerConverter, customFieldPersister);

        assertSame("foo", timeInStatusCFType.retrieveDatabaseValue(customField, issue));
    }

    public void testValueNotIndexedIfIssueIsUnresolved()
    {
        final StringBuffer valueClearedFromDb = new StringBuffer(Boolean.FALSE.toString());
        final String dbValueToBeCleared = "dummyValue";

        TimeInStatusCFType timeInStatusCFType = new TimeInStatusCFType(datePickerConverter, customFieldPersister)
        {
            protected String retrieveDatabaseValue(CustomField field, Issue issue)
            {
                return dbValueToBeCleared;
            }

            protected void clearDatabaseValue(CustomField field, Issue issueUnresolved, String dbString)
            {
                assertSame(customField, field);
                assertSame(issue, issueUnresolved);
                assertEquals(dbValueToBeCleared, dbString);

                valueClearedFromDb.setLength(0);
                valueClearedFromDb.append(Boolean.TRUE);
            }
        };


        assertNull(timeInStatusCFType.getValueFromIssue(customField, issue));
        assertTrue(Boolean.valueOf(valueClearedFromDb.toString()).booleanValue());
    }

    protected class TimeInStatusCFType extends com.atlassian.jira.ext.charting.field.TimeInStatusCFType {
        
        public TimeInStatusCFType(DatePickerConverter dateConverter, CustomFieldValuePersister customFieldValuePersister) {
            super(dateConverter, customFieldValuePersister);
        }

        protected TimeInStatusDAO getTimeInStatusDAO(DelegatorInterface delegatorInterface) {
            return new TestTimeInStatusDAO(delegatorInterface);
        }
    }

    protected class TestTimeInStatusDAO extends TimeInStatusDAO {

        public TestTimeInStatusDAO(final DelegatorInterface delegatorInterface) {
            super(delegatorInterface);
        }

        public String calculateForStatuses(Issue issue) {
            return timeInStatusToReturnByTimeInStatusDAO;
        }

        protected String getTableName(String entityName) {
            return entityName;
        }

        protected String getColName(String entityName, String fieldName) {
            return entityName + "." + fieldName;
        }
    }
}
