package com.atlassian.jira.ext.charting.field;

import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.util.EasyList;
import junit.framework.TestCase;
import org.apache.commons.lang.StringUtils;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.ofbiz.core.entity.DelegatorInterface;
import org.ofbiz.core.entity.GenericDataSourceException;
import org.ofbiz.core.entity.jdbc.SQLProcessor;
import org.ofbiz.core.entity.model.ModelEntity;
import org.ofbiz.core.entity.model.ModelField;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;

import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.when;

public class TestTimeInStatusDao extends TestCase {

    private static final String CHANGEGROUP_TABLE_ALIAS = "cg";
    private static final String CHANGEITEM_TABLE_ALIAS = "ci";
    
    private TimeInStatusDAO timeInStatusDAO;
    
    private String openStatusName = "Open";
    private String inProgressStatusName = "In Progress";
    private String fixedStatusName = "Fixed";
    private String reopenStatusName = "Reopen";
    
    private Collection<Status> statuses;
    
    @Mock private DelegatorInterface delegatorInterface;
    @Mock private ResultSet resultSet;
    @Mock private Issue issue;
    @Mock private ConstantsManager constantsManager;
    @Mock private Status openStatus;
    @Mock private Status inProgressStatus;
    @Mock private Status fixedStatus;
    @Mock private Status reopenStatus;

    protected void setUp() throws Exception {
        super.setUp();

        MockitoAnnotations.initMocks(this);
        
        statuses = new ArrayList<Status>();
        
        when(openStatus.getName()).thenReturn(openStatusName);
        when(openStatus.getId()).thenReturn(String.valueOf(openStatusName.hashCode()));
        statuses.add(openStatus);

        when(inProgressStatus.getName()).thenReturn(inProgressStatusName);
        when(inProgressStatus.getId()).thenReturn(String.valueOf(inProgressStatusName.hashCode()));
        statuses.add(inProgressStatus);
    }

    protected TimeInStatusDAO getTimeInStatusDAO(
            final DelegatorInterface delegatorInterface) {
        return new TimeInStatusDAO(delegatorInterface);
    }

    public void testCalculateForStatusesWithEmptyResultSet() throws SQLException {
        final MockSQLProcessor sqlProcessor = new MockSQLProcessor();
        final Long issueId = new Long(1);

        when(issue.getId()).thenReturn(issueId);

        when(resultSet.next()).thenReturn(Boolean.FALSE);
        sqlProcessor.setResultSet(resultSet);
        
        timeInStatusDAO = new TimeInStatusDAO(delegatorInterface);
        timeInStatusDAO.setSQLProcessor(sqlProcessor);

        /* For checking the generated SQL statements later */
        String changeGroup = timeInStatusDAO.getTableName("ChangeGroup");
        String changeItem = timeInStatusDAO.getTableName("ChangeItem");
        String cg_created = timeInStatusDAO.getColName("ChangeGroup", "created");
        String cg_issueid = timeInStatusDAO.getColName("ChangeGroup", "issue");
        String cg_id = timeInStatusDAO.getColName("ChangeGroup", "id");
        String ci_field = timeInStatusDAO.getColName("ChangeItem", "field");
        String ci_oldvalue = timeInStatusDAO.getColName("ChangeItem", "oldvalue");
        String ci_newvalue = timeInStatusDAO.getColName("ChangeItem", "newvalue");
        String ci_fieldtype = timeInStatusDAO.getColName("ChangeItem", "fieldtype");
        String ci_gid = timeInStatusDAO.getColName("ChangeItem", "group");


        assertNull(timeInStatusDAO.calculateForStatuses(issue));
        assertEquals(
                "select " + CHANGEGROUP_TABLE_ALIAS + "." + cg_created + ", " +
                CHANGEITEM_TABLE_ALIAS + "." + ci_oldvalue + ", " +
                CHANGEITEM_TABLE_ALIAS + "." + ci_newvalue + " " +
                " from " + changeGroup + " " + CHANGEGROUP_TABLE_ALIAS + " , " +
                        changeItem + " " + CHANGEITEM_TABLE_ALIAS +
                " where " + CHANGEGROUP_TABLE_ALIAS + "." + cg_issueid + " = ? " +
                " and " + CHANGEGROUP_TABLE_ALIAS + "." + cg_id + " = " + CHANGEITEM_TABLE_ALIAS + "." + ci_gid +
                " and " + CHANGEITEM_TABLE_ALIAS + "." + ci_fieldtype + "='jira' " +
                " and " + CHANGEITEM_TABLE_ALIAS + "." + ci_field + "='status' " +
                " order by " + CHANGEGROUP_TABLE_ALIAS + "." + cg_created + " asc",
                sqlProcessor.getSql());
        assertEquals(issueId, sqlProcessor.getIssueId());
        assertTrue(sqlProcessor.isClosed());
    }

    public void testCalculateForStatusesWithEmptyNotEmptyResultSet() throws SQLException {
        final MockSQLProcessor sqlProcessor = new MockSQLProcessor();
        final Long issueId = new Long(1);
        final Object[][] resultSetData = new Object[][] {
                { new Timestamp(1), "Open", "In Progress" },
                { new Timestamp(2), "In Progress", "Fixed" },
                { new Timestamp(3), "Fixed", "Fixed" },
                { new Timestamp(5), "Fixed", "Reopen" }
        };
        
        when(issue.getId()).thenReturn(issueId);
        when(issue.getCreated()).thenReturn(new Timestamp(0));
        
        when(resultSet.next()).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet.getTimestamp(1)).thenReturn((Timestamp) resultSetData[0][0]).thenReturn((Timestamp) resultSetData[1][0]).thenReturn((Timestamp) resultSetData[2][0]).thenReturn((Timestamp) resultSetData[3][0]);
        when(resultSet.getString(2)).thenReturn((String) resultSetData[0][1]).thenReturn((String) resultSetData[1][1]).thenReturn((String) resultSetData[2][1]).thenReturn((String) resultSetData[3][1]);
        when(resultSet.getString(3)).thenReturn((String) resultSetData[0][2]).thenReturn((String) resultSetData[1][2]).thenReturn((String) resultSetData[2][2]).thenReturn((String) resultSetData[3][2]);
        sqlProcessor.setResultSet(resultSet);
        
        timeInStatusDAO = new TimeInStatusDAO(delegatorInterface);
        timeInStatusDAO.setSQLProcessor(sqlProcessor);

        final String expectedResult = new StringBuffer()
                .append("Open").append(TimeInStatusDAO.STATUS_VALUES_SEPARATOR).append(1).append(TimeInStatusDAO.STATUS_VALUES_SEPARATOR).append(1)
                .append(TimeInStatusDAO.STATUSES_SEPARATOR).append("In Progress").append(TimeInStatusDAO.STATUS_VALUES_SEPARATOR).append(1).append(TimeInStatusDAO.STATUS_VALUES_SEPARATOR).append(1)
                .append(TimeInStatusDAO.STATUSES_SEPARATOR).append("Fixed").append(TimeInStatusDAO.STATUS_VALUES_SEPARATOR).append(3).append(TimeInStatusDAO.STATUS_VALUES_SEPARATOR).append(2)
                .append(TimeInStatusDAO.STATUSES_SEPARATOR).append("Reopen").append(TimeInStatusDAO.STATUS_VALUES_SEPARATOR).append(1).append(TimeInStatusDAO.STATUS_VALUES_SEPARATOR).append(0)
                .toString();
        final String calculatedResult = timeInStatusDAO.calculateForStatuses(issue);

        final List<String> expectedResultTokens = Arrays.asList(StringUtils.split(expectedResult, TimeInStatusDAO.STATUS_VALUES_SEPARATOR));
        final List<String> calculatedResultTokens = Arrays.asList(StringUtils.split(calculatedResult, TimeInStatusDAO.STATUS_VALUES_SEPARATOR));

        assertEquals(expectedResultTokens.size(), calculatedResultTokens.size());
        assertTrue(expectedResultTokens.containsAll(calculatedResultTokens));
    }

    public void testGetNumberOfTimesInStatus() {
        
        String customFieldValue;

        customFieldValue = new StringBuffer()
                .append("Open").append(TimeInStatusDAO.STATUS_VALUES_SEPARATOR).append(1).append(TimeInStatusDAO.STATUS_VALUES_SEPARATOR).append(1)
                .append(TimeInStatusDAO.STATUSES_SEPARATOR).append("In Progress").append(TimeInStatusDAO.STATUS_VALUES_SEPARATOR).append(1).append(TimeInStatusDAO.STATUS_VALUES_SEPARATOR).append(1)
                .append(TimeInStatusDAO.STATUSES_SEPARATOR).append("Fixed").append(TimeInStatusDAO.STATUS_VALUES_SEPARATOR).append(3).append(TimeInStatusDAO.STATUS_VALUES_SEPARATOR).append(2)
                .append(TimeInStatusDAO.STATUSES_SEPARATOR).append("Reopen").append(TimeInStatusDAO.STATUS_VALUES_SEPARATOR).append(1).append(TimeInStatusDAO.STATUS_VALUES_SEPARATOR).append(0)
                .toString();

        setupStatusStubs();

        when(constantsManager.getStatusObjects()).thenReturn(statuses);
        
        TimeInStatusDAO.setConstantsManager(constantsManager);
        assertEquals(new Double(3), TimeInStatusDAO.getNumberOfTimesInStatus(String.valueOf("Fixed".hashCode()), customFieldValue));
        assertEquals(new Double(2), TimeInStatusDAO.getSecondsInStatus(String.valueOf("Fixed".hashCode()), customFieldValue));
    }

    private void setupStatusStubs()
    {
        when(fixedStatus.getName()).thenReturn(fixedStatusName);
        when(fixedStatus.getId()).thenReturn(String.valueOf(fixedStatusName.hashCode()));
        statuses.add(fixedStatus);

        when(reopenStatus.getName()).thenReturn(reopenStatusName);
        when(reopenStatus.getId()).thenReturn(String.valueOf(reopenStatusName.hashCode()));
        statuses.add(reopenStatus);
    }

    public void testConvertNamesToStatusIds() {
        setupStatusStubs();

        when(constantsManager.getStatusObjects()).thenReturn(statuses);

        TimeInStatusDAO.setConstantsManager(constantsManager);

        List<String> statusNames;

        statusNames = TimeInStatusDAO.convertNamesToStatusIds(EasyList.build("A Non-Existing Status"));
        assertEquals(0, statusNames.size());

        statusNames = TimeInStatusDAO.convertNamesToStatusIds(EasyList.build("Fixed"));
        assertEquals(1, statusNames.size());
        assertEquals("Fixed", statusNames.get(0));

        statusNames = EasyList.build(String.valueOf("Fixed".hashCode()));
        assertSame(statusNames, statusNames);
    }

    public void testGetStatusIdMap() {
        when(constantsManager.getStatusObjects()).thenReturn(statuses);

        TimeInStatusDAO.setConstantsManager(constantsManager);
        final Map statusIdMap = TimeInStatusDAO.getStatusIdMap();

        assertTrue(statusIdMap.containsKey(String.valueOf("Open".hashCode())));
        assertTrue(statusIdMap.containsKey(String.valueOf("In Progress".hashCode())));
    }

    public void testIsInteger() {
        assertTrue(TimeInStatusDAO.isInteger("1"));
        assertFalse(TimeInStatusDAO.isInteger("1.0"));
        assertFalse(TimeInStatusDAO.isInteger("foo"));
        assertFalse(TimeInStatusDAO.isInteger(" 1 "));
        assertFalse(TimeInStatusDAO.isInteger(String.valueOf(Long.MAX_VALUE)));
    }

    public void testGetTableName() {
        when(delegatorInterface.getModelEntity(anyString())).thenReturn(
                new ModelEntity() {
                    public String getTableName(String string) {
                        return "fooTable";
                    }
                    
                    public ModelField getField(String string) {
                        return new ModelField() {
                            public String getColName() {
                                return "fooColumn";
                            }
                        };
                    }
                });

        final com.atlassian.jira.ext.charting.field.TimeInStatusDAO timeInStatusDAO =
                new com.atlassian.jira.ext.charting.field.TimeInStatusDAO(delegatorInterface);
        assertEquals("fooTable", timeInStatusDAO.getTableName("foo"));
    }

    public void testGetColumnName() {
        when(delegatorInterface.getModelEntity(anyString())).thenReturn(
                new ModelEntity() {
                    public ModelField getField(String string) {
                        return new ModelField() {
                            public String getColName() {
                                return "fooColumn";
                            }
                        };
                    }
                });

        final com.atlassian.jira.ext.charting.field.TimeInStatusDAO timeInStatusDAO =
                new com.atlassian.jira.ext.charting.field.TimeInStatusDAO(delegatorInterface);
        assertEquals("fooColumn", timeInStatusDAO.getColName("fooEntity", "fooField"));
    }

    public void testNumberOfTimesInStatusNotCalculatedIfTheStatusIsNotValidAnymore()
    {
        TimeInStatusDAO.setConstantsManager(constantsManager);

        when(constantsManager.getStatusObjects()).thenReturn(new HashSet());

        assertNull(TimeInStatusDAO.getNumberOfTimesInStatus(
                "Open", "Invalid Status" + TimeInStatusDAO.STATUS_VALUES_SEPARATOR + "1" + TimeInStatusDAO.STATUS_VALUES_SEPARATOR + "1" + TimeInStatusDAO.STATUSES_SEPARATOR
        ));
    }

    public void testNumberOfSecondsInStatusNotCalculatedIfTheStatusIsNotValidAnymore()
    {
        TimeInStatusDAO.setConstantsManager(constantsManager);

        when(constantsManager.getStatusObjects()).thenReturn(new HashSet());

        assertNull(TimeInStatusDAO.getNumberOfTimesInStatus(
                "Open", "Invalid Status" + TimeInStatusDAO.STATUS_VALUES_SEPARATOR + "1" + TimeInStatusDAO.STATUS_VALUES_SEPARATOR + "1" + TimeInStatusDAO.STATUSES_SEPARATOR
        ));
    }

    private static class MockSQLProcessor extends SQLProcessor {

        private String sql;

        private Long issueId;

        private ResultSet resultSet;

        private boolean closed;

        public MockSQLProcessor() {
            super("entityDs");
        }

        public String getSql() {
            return sql;
        }

        public Long getIssueId() {
            return issueId;
        }

        public ResultSet getResultSet() {
            return resultSet;
        }

        public void setResultSet(ResultSet resultSet) {
            this.resultSet = resultSet;
        }

        public boolean isClosed() {
            return closed;
        }

        public void prepareStatement(String sql) {
            this.sql = sql;
        }

        public void setValue(Long value) throws SQLException {
            issueId = value;
        }

        public ResultSet executeQuery() throws GenericDataSourceException {
            return resultSet;
        }

        public void close() throws GenericDataSourceException {
            closed = true;
        }
    }


    private static class TimeInStatusDAO extends com.atlassian.jira.ext.charting.field.TimeInStatusDAO {

        private SQLProcessor SQLProcessor;

        public TimeInStatusDAO(final DelegatorInterface delegatorInterface) {
            super(delegatorInterface);
        }

        public SQLProcessor getSQLProcessor() {
            return SQLProcessor;
        }

        public void setSQLProcessor(SQLProcessor SQLProcessor) {
            this.SQLProcessor = SQLProcessor;
        }

        protected String getTableName(String entityName) {
            return entityName;
        }

        protected String getColName(String entityName, String fieldName) {
            return entityName + "." + fieldName;
        }

        public static void setConstantsManager(final ConstantsManager constantsManager) {
            TimeInStatusDAO.constantsManager = constantsManager;
        }
    }
}
