package it.com.atlassian.jira.ext.charting;

import com.atlassian.jira.nimblefunctests.framework.NimbleFuncTestCase;
import com.atlassian.jira.testkit.client.Backdoor;
import com.atlassian.jira.testkit.client.util.TestKitLocalEnvironmentData;

public abstract class AbstractChartTestCase extends NimbleFuncTestCase
{
    public static final String ISSUE_TYPE_BUG = "Bug";
    public static final String ISSUE_TYPE_NEWFEATURE = "New Feature";
    public static final String ISSUE_TYPE_IMPROVEMENT = "Improvement";

    public static final String TEST_PROJECT_KEY = "TST";

    public static final String TEST_PROJECT_NAME = "Test Project";

    static final String TEST_PROJECT_ID = "10000";

    static final String TEST_PROJECT_PROJECT_FILTER_DESIGNATION = "project-" + TEST_PROJECT_ID;

    protected Backdoor backdoor;

    public void setUpTest()
    {
        administration.restoreData("testdata-export.xml");
        backdoor = new Backdoor(new TestKitLocalEnvironmentData());
    }

    void selectProjectOrFilterId(final String projectOrFilterDesignation)
    {
        if (!projectOrFilterDesignation.matches("(project|filter)-\\d+"))
        {
            throw new IllegalArgumentException("the project or filter designation is not in a valid format");
        }
        tester.setFormElement("projectOrFilterId", projectOrFilterDesignation);
    }

    protected void assertChartImagePresent()
    {
        tester.assertTextPresent("<img src='" + environmentData.getContext() + "/charts?filename=jfreechart-onetime-");
    }
}
